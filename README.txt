jQuery Msg Alert

This module displays Drupal's messages as a little alert at the bottom corner. Every message sent by drupal_set_message() will be converted to a dialog alert, using jQuery Msg Alert plugin. 
You can configure the time interval between then and they will reorganize themselves to be always near each other.
These alerts are actually jQuery UI dialogs, so you have a huge option of themes to customize the look and feel.

REQUIREMENTS:
jQuery Update - http://drupal.org/project/jquery_update
jQuery UI - http://drupal.org/project/jquery_ui
First, install jQuery Update and jQuery UI modules at sites/all/modules.
After enabling them, you need to download jQuery UI 1.7:
http://code.google.com/p/jquery-ui/downloads/list?q=1.7
And extract it at your sites/all/libraries. 
Remember to rename the folder to only "jquery.ui".
You can read more info about this level at README from jQuery UI module.

INSTALLATION:
Copy the folder to your sites/all/modules and enable jQuery Msg Alert. 
Go to admin/settings/jquery_msg_alert and make sure that you have the correct path to jQuery UI 1.7. If you do, you'll see a list of themes, choose one and configure the jquery plugin options as you wish.
You can download a lot of themes at:
http://jqueryui.com/themeroller/
This module has two permissions, one to administer the settings and the other to be able to see the dialogs alerts. 
If your user isn't seeing these alerts, make sure he has the permission.

jQuery Msg Alert's plugin page:
http://carvalhar.com/componente/jQueryMsgAlert/index.html